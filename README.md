# Description

This is a systemd timer for gpodder synchronization. It uses the commandline gpo

# Dependencies

* gpodder

# License

The license is BSD-2 to give the maximum permission for hacking and sharing.

However, if one day we have a license that forbids AI exploration in commercial
use such as copilot, I may change the license.
